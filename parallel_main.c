#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mpi.h>
#include "dalloc.h"

void import_JPEG_file(const char *filename, unsigned char **image_chars,int *image_height, int *image_width,int *num_components);
void export_JPEG_file(const char *filename, unsigned char *image_chars,int image_height, int image_width,int num_components, int quality);

typedef struct{
	float** image_data;
	int m;
	int n;
}image;

void *dalloc (size_t m_memb, size_t n_memb, size_t size);
void allocate_image(image *u, int m, int n);
void deallocate_image(image *u);
void convert_jpeg_to_image(const unsigned char* image_chars, image *u);
void convert_image_to_jpeg(const image *u, unsigned char* image_chars);
void printImage(image *u, int m, int n);    //helpefunksjon for å skrive ut bilde som flyttall
void iso_diffusion_denoising(image *u, image *u_bar, float kappa, int iters);
void d(char *);
void df(char *, int);
int hasRest(int m, int rank);

#define IMAGE_PART 999
#define ROW_UPDATE 888
#define SMOOTH_IMAGE 200
#define DEBUG 1

int MYRANK = -1;
int NUM_PROCS = -1;
int OVERLAP = 2;

int main(int argc, char* argv[]) {
	int i,j,k;
	int m, n, c, iters;
	int my_m,my_n,my_rank,num_procs;
	float kappa;
	image u, u_bar, whole_image;
	unsigned char *image_chars, *my_image_chars;
	char *input_jpeg_filename, *output_jpeg_filename;

	MPI_Init(&argc,&argv);
	MPI_Comm_rank(MPI_COMM_WORLD,&my_rank);
	MPI_Comm_size(MPI_COMM_WORLD,&num_procs);
	MPI_Status status;

	MYRANK = my_rank;	//global proc id
	NUM_PROCS = num_procs;	//global antall prosesser

	kappa=atof(argv[1]);
	iters=atoi(argv[2]);
	input_jpeg_filename=argv[3];
	output_jpeg_filename=argv[4];

	if(my_rank==0){
		import_JPEG_file(input_jpeg_filename, &image_chars, &m, &n, &c);
		allocate_image(&whole_image,m,n);
	}

	MPI_Bcast(&m,1,MPI_INT,0,MPI_COMM_WORLD);
	MPI_Bcast(&n,1,MPI_INT,0,MPI_COMM_WORLD);

	//(m/num_proc) indikerer at bilde blir delt opp i rader
    my_m = m / num_procs;
    if(my_rank != 0 && hasRest(m, my_rank))//ønsker ikke å legge til rest på siste proc dersom oddetall
        my_m += 1;

	my_n = n;
    
	if(my_rank != 0)
		my_m += OVERLAP;

	allocate_image(&u, my_m, my_n);
	allocate_image(&u_bar, my_m, my_n);
	my_image_chars=malloc(my_m*my_n*sizeof(char)); //alle må ha denne allokert!
	
	if(my_rank == 0){
		k = (my_m - OVERLAP) * my_n;

        int size = 0; 
		for(i = 1; i < num_procs; i++){
            if(hasRest(m, i) > 0) // Hvis prosessen skal ha en rest rad
                size = (my_m + OVERLAP + 1) * my_n; 
            else
                size = (my_m + OVERLAP) * my_n;

			MPI_Send(image_chars+k, size, MPI_CHAR, i, IMAGE_PART, MPI_COMM_WORLD);
            // For alle de andre prosessene blir det
            // (Prosess n > 1).m = (root).m + OVERLAP
            // (Prosess n > 1).m - OVERLAP blir derfor = (root).m
			k = my_m * my_n * (i + 1);
		}
		//legger inn første del av bildet i root
		for(i=0;i<my_m*my_n;i++)
			my_image_chars[i] = image_chars[i];
	}
	else{
		MPI_Recv(my_image_chars, my_m*my_n, MPI_CHAR, 0, IMAGE_PART, MPI_COMM_WORLD, &status); 
	}

	convert_jpeg_to_image(my_image_chars,&u);
	free(my_image_chars);
	iso_diffusion_denoising(&u, &u_bar,kappa,iters);

	//1. Alle  sender u_bar til proc 0
	if(my_rank!=0){
		MPI_Send(u_bar.image_data[0], u_bar.m*u_bar.n, MPI_FLOAT, 0, SMOOTH_IMAGE, MPI_COMM_WORLD);
	}
	// 2. proc 0 recv en del av u_bar fra alle proc
	else{	
		int i,j,k,x,y;
		
		//legger inn første del av bildet glattet del-bilde:
		for(i=0;i<u_bar.m;i++)	
			for(j=0;j<u_bar.n;j++)
				whole_image.image_data[i][j]=u_bar.image_data[i][j];
	
		//indeksene skal brukes i neste løkke
		j = 0;		
		i -= OVERLAP - 1;

		//k avgjør hvor hvem proc id u_bar kommer fra
		for(k=1;k<num_procs;k++){
            int new_m = my_m + OVERLAP;

            if(hasRest(m, k))
                new_m += 1;
                
		    deallocate_image(&u_bar);
		    allocate_image(&u_bar, new_m, my_n);

			MPI_Recv(u_bar.image_data[0], u_bar.m*u_bar.n, MPI_FLOAT, k, SMOOTH_IMAGE, MPI_COMM_WORLD, &status);

			// 3. map nye verdier fra u.bar tilbake til whole_image
			for(x=1;x<u_bar.m;x++){	
				for(y=0;y<u_bar.n;y++){	
					whole_image.image_data[i][j] = u_bar.image_data[x][y];
					j+=1;
				}
				j=0;
				i+=1;
			}

			i -= OVERLAP - 1;
		}

		convert_image_to_jpeg(&whole_image,image_chars);
		export_JPEG_file(output_jpeg_filename,image_chars,m,n,c,75);
		free(image_chars);
		deallocate_image(&whole_image);
		d("Denoising finished");
	}

	deallocate_image(&u);
	deallocate_image(&u_bar);
	MPI_Finalize();
	return 0;
}

void allocate_image(image *u, int m, int n){
	u->m=m;
	u->n=n;
	u->image_data = dalloc(m,n,sizeof(float));
}

void deallocate_image(image *u){
	dalloc_free(u->image_data);
}

void convert_jpeg_to_image(const unsigned char* image_chars, image *u){
	int i,j;
	int k=0;
	for(i=0; i<u->m;i++)
		for(j=0; j<u->n;j++){
			
			u->image_data[i][j]=image_chars[k++];
		}	
	//printf("convert JPEG to image finished\n");
}
void convert_image_to_jpeg(const image *u, unsigned char* image_chars){
	int i,j;
	int k=0;
	for(i=0; i<u->m;i++)
		for(j=0; j<u->n;j++){
			image_chars[k++]=u->image_data[i][j];
		}
	//printf("convert image to JPEG finished\n");
}
void printImage(image *u, int m, int n){
	int row,column;
	for(row=0;row<m; row++){
		for(column=0;column<n; column++){
			printf("%5.0f ",u->image_data[row][column]);
		}
		printf("\n");
	}
}
void imgCopy(image *src, image *dst){
		int i,j;
		for(i = 0; i < src->m; i++)
			for(j = 0; j < src->n; j++)
				dst->image_data[i][j] = src->image_data[i][j];
}

void iso_diffusion_denoising(image *u, image *u_bar, float kappa, int iters){
	int i,j,k,l;
	int dst=-1;
	int my_m=u->m;
	int my_n=u->n;
	
	imgCopy(u,u_bar);

	for(k=0;k<iters;k++){
		if(MYRANK != 0)
			MPI_Recv(u->image_data[0], u->n, MPI_FLOAT, MYRANK - 1, ROW_UPDATE, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

		if(k != 0 && MYRANK != NUM_PROCS - 1)
			MPI_Recv(u->image_data[u->m-1], u->n, MPI_FLOAT, MYRANK + 1, ROW_UPDATE, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

		for(i=1;i<=my_m-2;i++){
			for(j=1;j<=my_n-2;j++){
				u_bar->image_data[i][j] = u->image_data[i][j] + kappa *
					(u->image_data[i-1][j] +
					 u->image_data[i][j-1] - 4 *
					 u->image_data[i][j] +
					 u->image_data[i+1][j] +
					 u->image_data[i][j+1]);
			}

		}	
		if(MYRANK != 0 && k != iters - 1)
			MPI_Send(u->image_data[1], u->n, MPI_FLOAT, MYRANK - 1,ROW_UPDATE, MPI_COMM_WORLD);
		if(MYRANK != NUM_PROCS - 1)
			MPI_Send(u->image_data[u->m-2], u->n, MPI_FLOAT, MYRANK + 1, ROW_UPDATE, MPI_COMM_WORLD);

		imgCopy(u_bar,u);
	}
	//printf("denoising finished!\n");
}
void d(char *msg) {
	if(DEBUG) {
		char *postfix = ", proc: %d\n";
		char *string = malloc(strlen(msg) + strlen(postfix)+1);
		strcpy(string, msg);
		strcat(string, postfix);

		fprintf(stderr,string, MYRANK);
	}
}
void df(char *msg, int integer){
	if(DEBUG){
		char *postfix = ", proc: %d\n";
		char *string = malloc(strlen(msg) + strlen(postfix)+1);
		strcpy(string, msg);
		strcat(string, postfix);	

		fprintf(stderr,string, integer, MYRANK);
	}
}

void *dalloc (size_t m_memb, size_t n_memb, size_t size)
{
  size_t i;
  void ** retval;

  retval = malloc(m_memb * sizeof(void *));

  if (retval == NULL)
    return NULL;

  retval[0] = malloc(m_memb * n_memb * size);

  if (retval[0] == NULL) {
    free(retval);
    return NULL;
  }

  for (i = 1; i < m_memb; i++)
    retval[i] =
      ((char *) retval[0]) + i * n_memb * size;

  return retval;
}
void dalloc_free (void * ptr)
{
  if (ptr == NULL)
    return;

  free(* (void **) ptr);
  free(ptr);
}
int hasRest(int m, int rank) {
    /* Denne sjekker om en prosess har en rest rad utifra om m % NUM_PROCS har 
       rest og hvilken rank det er. I tilfellet hvor X % Y er 2, og man er i
       prosess med oddetall, så bør ikke denne prosessen få en rest rad da alle rest
       radene er distribuert. */
    return (m % NUM_PROCS) - rank > 0;
}
